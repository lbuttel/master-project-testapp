package de.fhkl.buttel;

import de.fhkl.buttel.windows.Window1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class MainWindow {

    private void createUIComponents() {
        JFrame mainFrame = new JFrame("Update Demo");
        mainFrame.setSize(250, 200);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Container pane = mainFrame.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        JButton btnWindow1 = new JButton("öffne 1. Fenster");
        btnWindow1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Window1().showWindow();
            }
        });
        pane.add(btnWindow1);
        btnWindow1.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnWindow1.setAlignmentY(Component.BOTTOM_ALIGNMENT);

//        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public static void main(String... args) {
        new MainWindow().createUIComponents();
    }
}
