package de.fhkl.buttel.windows;

import org.jdesktop.swingx.JXFrame;
import org.jdesktop.swingx.JXStatusBar;

import javax.swing.*;
import java.awt.*;

public class Window1 {

    private void createUIComponents() {
        JXFrame frame1 = new JXFrame("1. Fenster");
        frame1.setSize(250, 200);
        frame1.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        Container pane = frame1.getContentPane();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        JLabel lblWindow1 = new JLabel("Text");
        pane.add(lblWindow1);

        JLabel lbl2 = new JLabel("anderer Text");
        pane.add(lbl2);

        JXStatusBar statusBar = new JXStatusBar();
        JLabel progressLabel = new JLabel("Progress");
        final JLabel endLabel = new JLabel("");
        final JProgressBar pb = new JProgressBar(0, 100);
        progressLabel.setLabelFor(pb);
        statusBar.add(progressLabel);
        statusBar.add(pb);
        statusBar.add(endLabel);
        Thread t = new Thread(){
            public void run(){
                int i=0;
                while (i<=100){
                    pb.setValue(++i);
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(pb.getMaximum() == i) endLabel.setText("fertig!");
                }
            }
        };
        t.start();
        frame1.setStatusBar(statusBar);
        frame1.setStartPosition(JXFrame.StartPosition.CenterInScreen);
        frame1.setSize(300, 300);
        frame1.setVisible(true);

        lblWindow1.setAlignmentX(Component.CENTER_ALIGNMENT);
        lbl2.setAlignmentX(Component.CENTER_ALIGNMENT);

//        frame1.pack();
        frame1.setVisible(true);
    }

    public void showWindow() {
        createUIComponents();
    }
}
